# Buehler candidate-test

Hi, 

Here is the small MVVM example. Most of our projects are built in MVVM architecture.

For this test, a simple demo program allows a user to enter patient details which we will display in a list.

We have Id, Name and Mobile number fields already. You need to add Date of Birth field and also
when you click on add button mobile number and Date of birth should be displayed on the list . 

Ref: Add button is enabled only when you enter all fields and go back to Id field.

You will also find a build error bug when you run the project for the first time. This error should be debugged and fixed.

you can run this project in visual studio community edition.

Please don't send this project by email. Upload it to onedrive, Dropbox etc., or create your own bitbucket account and send us the link we 
will dowload your project.

Good Luck.